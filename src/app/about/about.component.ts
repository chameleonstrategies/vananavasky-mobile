import { Component, OnInit } from '@angular/core';
import { WordpressService } from '../core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  public team: any[] = [];

  constructor(private service: WordpressService) { }

  ngOnInit() {
    this.service.getTeam().then(team => {
      this.team = team;
    });
  }

}
