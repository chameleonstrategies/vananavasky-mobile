import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  HomeVideoComponent,
  HomeComponent,
  AboutComponent,
  ContactComponent,
  CrowdComponent,
  HappeningsComponent,
  HappeningDetailComponent,
  ExperienceDetailComponent,
  ExperiencesComponent,
  MediaComponent,
  NewsComponent,
} from "./index";
import { EventsResolve, ExperiencesResolve } from "./core/resolvers/index";

const routes: Routes = [
  {
    path: "experiences",
    resolve: { experiences: ExperiencesResolve },
    children: [
      { path: "", pathMatch: "full", component: ExperiencesComponent },
      { path: ":name", component: ExperienceDetailComponent },
    ],
  },
  {
    path: "sky-deck",
    redirectTo: '/experiences/sky-deck'
  },
  {
    path: "drink",
    redirectTo: '/experiences/drink'
  },
  {
    path: "culinary",
    redirectTo: '/experiences/culinary'
  },
  {
    path: "lounge",
    redirectTo: '/experiences/lounge'
  },
  {
    path: "happenings",
    resolve: { events: EventsResolve },
    children: [
      {
        path: "",
        pathMatch: "full",
        component: HappeningsComponent,
      },
      {
        path: ":index",
        component: HappeningDetailComponent,
      },
    ],
  },
  { path: "media", component: MediaComponent },
  { path: "news", component: NewsComponent },
  { path: "contact", component: ContactComponent },
  { path: "crowd", component: CrowdComponent },
  { path: "about", component: AboutComponent },
  { path: "team", component: AboutComponent },
  { path: "home", component: HomeComponent },
  { path: "", pathMatch: "full", component: HomeVideoComponent },
  { path: "**", redirectTo: "" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
