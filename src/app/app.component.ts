import { Component, ViewChild, OnInit, Inject, PLATFORM_ID, ViewEncapsulation } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { SharedService } from './core/services/shared.service';
import { isPlatformServer, Location, isPlatformBrowser } from '@angular/common';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

declare var ga: Function;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  public isHome: boolean = false;
  public backgroundUrl: SafeResourceUrl

  @ViewChild(MatSidenav, { static: true }) sidenav: MatSidenav;

  private get isBrowser(): boolean {
    return isPlatformBrowser(this.platformId);
  }

  private get isServer(): boolean {
    return isPlatformServer(this.platformId);
  }

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private router: Router,
    private location: Location,
    private sanitizer: DomSanitizer,
    private shared: SharedService) {
    this.backgroundUrl = this.sanitizer.bypassSecurityTrustResourceUrl("https://www.vananavasky.com/slider.php?id=mobile-homepage");
    //this.backgroundUrl = this.sanitizer.bypassSecurityTrustResourceUrl("https://www.vananavasky.com/mobile-slider.html");
    this.isHome = this.location.path() == '' || this.location.path() == '/';
    this.router.events.subscribe(ev => {
      if (ev instanceof NavigationEnd) {
        this.sidenav.close();
        this.scrollTop();
        this.isHome = ev.urlAfterRedirects == '/';

        if (this.isBrowser) {
          ga('set', 'page', ev.url);
          ga('send', 'pageview');
        }
      }
    });
  }

  ngOnInit() {
    this.shared.set("sidenav", this.sidenav);
  }

  private scrollTop() {
    if (this.isServer)
      return;

    const content = document.querySelector('.scroll-content');
    content.scrollTop = 0;
  }

}