import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as cache from 'lscache';

@Injectable()
export class AppConfig {
    constructor(private http: HttpClient) { }

    load() {
        return new Promise((resolve, reject) => {
            const url = 'assets/data/config.json';
            const data = cache.get(url);
            if (data) {
                resolve(data);
            } else {
                this.http.get<any[]>(url).subscribe(result => {
                    cache.set(url, result, 60);
                    resolve(result);
                }, err => reject(err));
            }
        });
    }
}