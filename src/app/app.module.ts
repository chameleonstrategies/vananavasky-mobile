import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, APP_INITIALIZER } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppConfig } from './app.config';
import { AppComponent } from './app.component';
import {
  HomeVideoComponent,
  HomeComponent,
  AboutComponent,
  ContactComponent,
  CrowdComponent,
  HappeningsComponent,
  HappeningDetailComponent,
  ExperienceDetailComponent,
  ExperiencesComponent,
  MediaComponent,
  NewsComponent
} from './index';

import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeVideoComponent,
    HomeComponent,
    AboutComponent,
    ContactComponent,
    CrowdComponent,
    HappeningsComponent,
    HappeningDetailComponent,
    ExperienceDetailComponent,
    ExperiencesComponent,
    MediaComponent,
    NewsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    SharedModule,
    AppRoutingModule
  ],
  providers: [
    //   {
    //   provide: APP_INITIALIZER,
    //   useFactory: (config: AppConfig) => () => config.load(),
    //   deps: [AppConfig],
    //   multi: true
    // },
    //   AppConfig
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
