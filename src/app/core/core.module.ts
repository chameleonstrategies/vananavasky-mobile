import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { TruncateModule } from 'ng2-truncate';
import {
  MatToolbarModule,
  MatButtonModule,
  MatInputModule,
  MatSidenavModule,
  MatProgressSpinnerModule,
  MatListModule,
  MatDividerModule
} from '@angular/material';
import {
  EnwokeService,
  LocalService,
  WordpressService
} from './services/index';
import {
  EventsResolve,
  ExperiencesResolve
} from './resolvers/index';
import { SharedService } from './services/shared.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    TruncateModule,
    MatToolbarModule,
    MatListModule,
    MatDividerModule,
    MatButtonModule,
    MatInputModule,
    MatSidenavModule,
    MatProgressSpinnerModule
  ],
  exports: [
    CommonModule,
    HttpClientModule,
    TruncateModule,
    MatToolbarModule,
    MatListModule,
    MatDividerModule,
    MatButtonModule,
    MatInputModule,
    MatSidenavModule,
    MatProgressSpinnerModule
  ],
  providers: [
    EnwokeService,
    LocalService,
    SharedService,
    WordpressService,
    EventsResolve,
    ExperiencesResolve
  ]
})
export class CoreModule { }
