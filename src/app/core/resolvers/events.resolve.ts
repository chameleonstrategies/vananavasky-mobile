
import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { WordpressService } from "../services/wordpress.service";
import { Observable } from "rxjs";

@Injectable()
export class EventsResolve implements Resolve<any[]> {

    constructor(private wordpress: WordpressService) { }

    resolve(): Observable<any[]> | Promise<any[]> {
        return this.wordpress.getHappenings();
    }
}