import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { LocalService } from "../services/local.service";
import { Observable } from "rxjs";

@Injectable()
export class ExperiencesResolve implements Resolve<any[]> {

    constructor(private local: LocalService) { }

    resolve(): Observable<any[]> | Promise<any[]> {
        return this.local.getExperiences();
    }
}