import { TestBed, inject } from '@angular/core/testing';

import { EnwokeService } from './enwoke.service';

describe('EnwokeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EnwokeService]
    });
  });

  it('should be created', inject([EnwokeService], (service: EnwokeService) => {
    expect(service).toBeTruthy();
  }));
});
