import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as cache from 'lscache';

@Injectable()
export class EnwokeService {
  endpoint = "https://www.enwoke.com/posts.json?campaign_id=3&all=1";

  constructor(private http: HttpClient) { }

  getPosts(options: {
    page: number,
    perPage: number
  }): Promise<any> {
    return new Promise((resolve, reject) => {
      const url = this.endpoint + `&page=${options.page}&per_page=${options.perPage}`;
      const data = cache.get(url);
      if (data) {
        resolve(data);
      } else {
        this.http.get(url).subscribe(result => {
          cache.set(url, result, 5);
          resolve(result);
        }, err => reject(err));
      }
    });
  }
}
