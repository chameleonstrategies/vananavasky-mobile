import { HttpClient } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import * as cache from 'lscache';

@Injectable()
export class LocalService {

  constructor(private http: HttpClient) { }

  getConfig(): Promise<any[]> {
    return new Promise((resolve, reject) => {
      const url = 'assets/data/config.json';
      const data = cache.get(url);
      if (data) {
        resolve(data);
      } else {
        this.http.get<any[]>(url).subscribe(result => {
          cache.set(url, result, 60);
          resolve(result);
        }, err => reject(err));
      }
    });
  }

  getExperiences(): Promise<any[]> {
    return new Promise((resolve, reject) => {
      const url = 'assets/data/experiences.json';
      const data = cache.get(url);
      if (data) {
        resolve(data);
      } else {
        this.http.get<any[]>(url).subscribe(result => {
          cache.set(url, result, 60);
          resolve(result);
        }, err => reject(err));
      }
    });
  }

  getTeam(): Promise<any[]> {
    return new Promise((resolve, reject) => {
      const url = 'assets/data/team.json';
      const data = cache.get(url);
      if (data) {
        resolve(data);
      } else {
        this.http.get<any[]>(url).subscribe(result => {
          cache.set(url, result, 60);
          resolve(result);
        }, err => reject(err));
      }
    });
  }
}
