import { HttpClient } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';

@Injectable()
export class SharedService {
    private dict: any = {};

  constructor(private http: HttpClient) { }

  get(key: string): any {
      return this.dict[key];
  }

  set(key: string, val: any) {
      this.dict[key] = val;
  }
}
