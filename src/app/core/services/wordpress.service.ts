import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as cache from 'lscache';

@Injectable()
export class WordpressService {

  public endpoint = "https://www.vananavasky.com/api/";

  constructor(private http: HttpClient) { }

  // Non-Caching version.
  getHappenings(): Promise<any[]> {
    return new Promise((resolve, reject) => {
      const url = this.endpoint + 'upcoming_events.php';
      this.http.get<any[]>(`${url}?_t=${Date.now()}`).subscribe(result => {
        result.forEach(it => {
          const st = new Date(it.start_time);
          const et = new Date(it.end_time);
          it.start_date = this.getDate(st);
          it.end_date = this.getDate(et);;
          it.start_time = this.getTime(st);
          it.end_time = this.getTime(et);
        });

        resolve(result);
      }, err => reject(err));
    });
  }

  //// Caching version.
  // getHappenings(): Promise<any[]> {
  //   return new Promise((resolve, reject) => {
  //     const url = this.endpoint + 'upcoming_events.php';
  //     const data = cache.get(url);
  //     if (data) {
  //       resolve(data);
  //     } else {
  //       this.http.get<any[]>(`${url}?_t=${Date.now()}`).subscribe(result => {
  //         result.forEach(it => {
  //           const st = new Date(it.start_time);
  //           const et = new Date(it.end_time);
  //           it.start_date = this.getDate(st);
  //           it.end_date = this.getDate(et);;
  //           it.start_time = this.getTime(st);
  //           it.end_time = this.getTime(et);
  //         });

  //         cache.set(url, result, 5);
  //         resolve(result);
  //       }, err => reject(err));
  //     }
  //   });
  // }

  getTeam(): Promise<any[]> {
    return new Promise((resolve, reject) => {
      const url = this.endpoint + 'team.php';
      const data = cache.get(url);
      if (data) {
        resolve(data);
      } else {
        this.http.get<any[]>(`${url}?_t=${Date.now()}`).subscribe(result => {
          cache.set(url, result, 5);
          resolve(result);
        }, err => reject(err));
      }
    });
  }

  getMedia(): Promise<any[]> {
    return new Promise((resolve, reject) => {
      const url = this.endpoint + 'media.php';
      const data = cache.get(url);
      if (data) {
        resolve(data);
      } else {
        this.http.get<any[]>(`${url}?_t=${Date.now()}`).subscribe(result => {
          result.forEach(it => {
            const d = new Date(it.date);
            it.date = this.getDate(d);
          });

          cache.set(url, result, 5);
          resolve(result);
        }, err => reject(err));
      }
    });
  }

  getPress(): Promise<any[]> {
    return new Promise((resolve, reject) => {
      const url = this.endpoint + 'press.php';
      const data = cache.get(url);
      if (data) {
        resolve(data);
      } else {
        this.http.get<any[]>(`${url}?_t=${Date.now()}`).subscribe(result => {
          result.forEach(it => {
            const d = new Date(it.date);
            it.date = this.getDate(d);
          });

          cache.set(url, result, 5);
          resolve(result);
        }, err => reject(err));
      }
    });
  }

  private getDate(d: Date) {
    return `${this.pad(d.getDate())}.${this.pad(d.getMonth() + 1)}.${d.getFullYear()}`;;
  }

  private getTime(d: Date) {
    return `${this.pad(d.getHours())}:${this.pad(d.getMinutes())}`;
  }

  private pad(num: number, digit: number = 2) {
    let s = '0'.repeat(digit) + num;
    return s.length == digit ? s : s.substring(s.length - 2);
  }
}
