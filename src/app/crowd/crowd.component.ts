import { isPlatformServer } from '@angular/common';
import { Component, HostListener, ElementRef, OnInit, OnDestroy, AfterViewInit, NgZone, Inject, PLATFORM_ID } from '@angular/core';
import { EnwokeService } from '../core';
import { DomSanitizer } from '@angular/platform-browser';

declare let jQuery: any;

@Component({
  selector: 'app-crowd',
  templateUrl: './crowd.component.html',
  styleUrls: ['./crowd.component.scss']
})
export class CrowdComponent implements OnInit, OnDestroy {

  public loaded: boolean = false;
  public loading: boolean = false;
  public posts: any[] = [];
  public final: boolean = true;
  public page: number = 1;
  public perPage: number = 10;
  public authorNameLength: number = 24;

  private pixelsBeforeLoad: number = 300;
  private $container: any = null;
  private scrollFn: any = null;

  get isServer(): boolean {
    return isPlatformServer(this.platformId);
  }

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private zone: NgZone,
    private element: ElementRef,
    private sanitizer: DomSanitizer,
    private enwoke: EnwokeService) { }

  ngOnInit() {
    this.loadData().then(() => {
      this.zone.run(() => {
        this.addPostsListEvent();
      })
    });
  }

  ngOnDestroy() {
    this.removePostsListEvent();
  }

  addPostsListEvent() {
    if (this.isServer)
      return;

    this.$container = jQuery('.scroll-content');
    this.scrollFn = ev => {
      this.scrollEventHandler(ev);
    };
    this.$container.bind('scroll', this.scrollFn);
  }

  scrollEventHandler(ev) {
    if (!this.loading && ev.target.clientHeight + ev.target.scrollTop >= ev.target.scrollHeight - this.pixelsBeforeLoad) {
      this.loadMore();
    }
  }

  removePostsListEvent() {
    if (this.$container) {
      this.$container.unbind('scroll', this.scrollFn);
    }
  }

  loadData(replace = false) {
    return new Promise((resolve, reject) => {
      if (this.loading)
        return resolve([]);

      this.loading = true;
      this.enwoke.getPosts({ page: this.page, perPage: this.perPage }).then(result => {
        this.loading = false;
        this.loaded = true;
        this.final = result.final;

        if (this.final) {
          this.removePostsListEvent();
        }

        if (result.posts && result.posts.length > 0) {
          result.posts.forEach(it => {
            it.network_name = it.network_name;
            if (it.video_url) {
              it.video_resource_url = this.sanitizer.bypassSecurityTrustResourceUrl(it.video_url);
            }
          });
        }
        if (replace === true) {
          this.posts = result.posts;
        } else {
          this.posts = this.posts.concat(result.posts);
        }
        resolve(result.posts);
      },
        err => {
          this.loading = false;
          this.loaded = true;
          reject(err);
        });
    });
  }

  loadMore() {
    if (!this.final) {
      this.page++;
      this.loadData();
    }
  }

}