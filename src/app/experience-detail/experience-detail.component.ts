import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-experience-detail',
  templateUrl: './experience-detail.component.html',
  styleUrls: ['./experience-detail.component.scss']
})
export class ExperienceDetailComponent implements OnInit {

  public experience: any;
  public canBack: boolean = false;
  public canNext: boolean = false;
  private experiences: any;
  private index: number = -1;

  constructor(private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.experiences = this.activatedRoute.snapshot.data['experiences'];
    this.activatedRoute.params.subscribe(params => {
      let name = params['name'];
      console.log(name);
      if (this.experiences && this.experiences.length > 0 && name) {
        name = name.trim();
        const exp = this.experiences.find(it => it.name == name);
        if (exp) {
          this.index = this.experiences.indexOf(exp);
          this.display();
        } else {
          this.router.navigate(['/']);
        }
      } else {
        this.router.navigate(['/']);
      }
    })
  }

  display() {
    this.experience = this.experiences[this.index];
    this.canBack = this.index > 0;
    this.canNext = this.index < this.experiences.length - 1;
  }

  goBack() {
    const name = this.experiences[this.index - 1].name;
    this.router.navigate(['/', 'experiences', name]);
  }

  goNext() {
    const name = this.experiences[this.index + 1].name;
    this.router.navigate(['/', 'experiences', name]);
  }

}
