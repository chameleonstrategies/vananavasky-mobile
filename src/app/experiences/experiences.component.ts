import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-experiences',
  templateUrl: './experiences.component.html',
  styleUrls: ['./experiences.component.scss']
})
export class ExperiencesComponent implements OnInit {

  public experiences: any[] = [];

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.experiences = this.activatedRoute.snapshot.data['experiences'];
    if (!this.experiences || this.experiences.length == 0) {
      this.router.navigate(['/']);
    }
  }

}
