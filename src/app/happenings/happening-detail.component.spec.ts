import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HappeningDetailComponent } from './happening-detail.component';

describe('HappeningDetailComponent', () => {
  let component: HappeningDetailComponent;
  let fixture: ComponentFixture<HappeningDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HappeningDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HappeningDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
