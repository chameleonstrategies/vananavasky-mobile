import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-happening-detail',
  templateUrl: './happening-detail.component.html',
  styleUrls: ['./happening-detail.component.scss']
})
export class HappeningDetailComponent implements OnInit {

  public loading: boolean = false;
  public event: any;
  public canBack: boolean = false;
  public canNext: boolean = false;
  public reservationLink: SafeUrl = null;
  private events: any;
  private index: number = -1;


  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.loading = true;
    this.events = this.activatedRoute.snapshot.data['events'];
    this.activatedRoute.params.subscribe(params => {
      this.loading = false;
      this.index = params['index'] - 1;
      if (this.events && this.events.length > 0 && this.index >= 0) {
        this.display();
      } else {
        this.router.navigate(['/']);
      }
    });

    this.reservationLink = this.sanitizer.bypassSecurityTrustUrl("https://www.opentable.co.th/r/vana-nava-sky-bar-reservations-hua-hin-district?restref=118061&lang=en-US&no-r3-redirect=1");
  }

  display() {
    this.event = this.events[this.index];
    this.canBack = this.index > 0;
    this.canNext = this.index < this.events.length - 1;
  }

  goBack() {
    this.router.navigate(['/', 'happenings', this.index]);
  }

  goNext() {
    this.router.navigate(['/', 'happenings', this.index + 2]);
  }

}
