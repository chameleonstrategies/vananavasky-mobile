import {
  Component,
  OnInit
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-happenings',
  templateUrl: './happenings.component.html',
  styleUrls: ['./happenings.component.scss']
})
export class HappeningsComponent implements OnInit {

  public events: any[] = [];
  public carouselOptions: any = {
    loop: true,
    margin: 10,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 5
      }
    }
  };

  constructor(private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    const events = this.activatedRoute.snapshot.data['events'];
    if (events && events.length > 0) {
      this.events = events;
    } else {
      this.router.navigate(['/']);
    }
  }

}
