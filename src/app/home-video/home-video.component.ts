import { Component, OnInit } from '@angular/core';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { LocalService } from '../core';

@Component({
    selector: 'app-home-video',
    templateUrl: 'home-video.component.html',
    styleUrls: ['home-video.component.scss']
})

export class HomeVideoComponent implements OnInit {
    public reservationLink: SafeUrl = "";

    constructor(
        private svLocal: LocalService,
        private sanitizer: DomSanitizer) { }

    ngOnInit() {
        this.reservationLink = this.sanitizer.bypassSecurityTrustUrl("https://www.opentable.co.th/r/vana-nava-sky-bar-reservations-hua-hin-district?restref=118061&lang=en-US&no-r3-redirect=1");
    }
}