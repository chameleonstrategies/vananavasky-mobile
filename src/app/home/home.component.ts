import { Component, OnInit, Inject, PLATFORM_ID, AfterViewInit } from '@angular/core';
import { SafeResourceUrl, DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { WordpressService } from '../core';
import { isPlatformServer } from '@angular/common';

declare let jQuery: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {
  public reservationLink: SafeUrl = "";
  public mapUrl: SafeResourceUrl = "";
  public loaded: boolean = false;
  public events: any[] = [];
  public carouselOptions: any = {
    center: true,
    loop: true,
    margin: 10,
    responsive: {
      0: {
        items: 2
      },
      600: {
        items: 3
      }
    }
  };

  private carousel: any;

  get isServer(): boolean {
    return isPlatformServer(this.platformId);
  }

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private sanitizer: DomSanitizer,
    private wordpress: WordpressService) { }

  ngOnInit() {

    this.reservationLink = this.sanitizer.bypassSecurityTrustUrl("https://www.opentable.co.th/r/vana-nava-sky-bar-reservations-hua-hin-district?restref=118061&lang=en-US&no-r3-redirect=1");
    this.mapUrl = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.google.com/maps/place/VANA+NAVA+SKY+BAR+%26+RESTAURANT/@12.5320913,99.9612656,17z/data=!3m1!4b1!4m5!3m4!1s0x30fdab8bde2c957f:0xbb358a84e1145bd1!8m2!3d12.5320913!4d99.9634543');
  }

  ngAfterViewInit() {
    this.wordpress.getHappenings()
      .then(result => {
        this.loaded = true;
        this.events = result;
      })
      .catch(err => {
        this.loaded = true;
        console.error(err);
      });
  }

}
