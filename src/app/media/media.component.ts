import { Component, OnInit } from '@angular/core';
import { WordpressService } from '../core';

@Component({
    selector: 'app-media',
    templateUrl: 'media.component.html',
    styleUrls: ['media.component.scss']
})

export class MediaComponent implements OnInit {
    public loaded: boolean = false;
    public media: any[] = [];

    constructor(private wordpress: WordpressService) { }

    ngOnInit() {
        this.wordpress.getMedia().then(media => {
            this.loaded = true;
            this.media = media;
        });
    }
}