import { Component, OnInit } from '@angular/core';
import { WordpressService } from '../core';

@Component({
    selector: 'app-news',
    templateUrl: 'news.component.html',
    styleUrls: ['news.component.scss']
})

export class NewsComponent implements OnInit {
    public loaded: boolean = false;
    public press: any[] = [];

    constructor(private wordpress: WordpressService) { }

    ngOnInit() {
        this.wordpress.getPress().then(press => {
            this.loaded = true;
            this.press = press;
        });}
}