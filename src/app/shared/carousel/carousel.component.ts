import {
    Component,
    AfterViewInit,
    OnDestroy,
    ElementRef,
    Input,
    Output,
    PLATFORM_ID,
    Inject
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

declare var jQuery: any;

@Component({
    selector: 'app-carousel',
    template: `
    <ng-content></ng-content>
    `
})
export class CarouselComponent implements AfterViewInit, OnDestroy {
    @Input() options: any = {
        loop: false,
        margin: 10,
        items: 1
    };
    @Input() items: any[];

    get isBrowser(): boolean {
        return isPlatformBrowser(this.platformId);
    }

    constructor(@Inject(PLATFORM_ID) private platformId: Object, private element: ElementRef) { }

    ngAfterViewInit() {
        if (this.isBrowser) {
            jQuery(this.element.nativeElement).addClass('owl-carousel').owlCarousel(this.options);
        }
    }

    ngOnDestroy() {
        if (this.isBrowser) {
            jQuery(this.element.nativeElement).owlCarousel('destroy').removeClass('owl-carousel');
        }
    }
}
