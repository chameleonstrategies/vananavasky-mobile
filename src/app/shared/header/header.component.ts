import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { SharedService } from '../../core/services/shared.service';
import { MatSidenav } from '@angular/material';

@Component({
    selector: 'app-header',
    templateUrl: 'header.component.html',
    styleUrls: ['header.component.scss']
})

export class HeaderComponent implements OnInit {
    @Input() showHomeLink: boolean = false;

    constructor(private shared: SharedService) { }

    ngOnInit() { }

    clickToggleMenu() {
        const sidenav: MatSidenav = this.shared.get('sidenav');
        if(sidenav) {
            sidenav.open();
        }
    }
}