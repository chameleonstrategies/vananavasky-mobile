import { CoreModule } from './../core/core.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent, CarouselComponent, SideMenuComponent } from './index';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    CoreModule
  ],
  declarations: [
    HeaderComponent,
    SideMenuComponent,
    CarouselComponent
  ],
  exports: [
    HeaderComponent,
    SideMenuComponent,
    CarouselComponent
  ]
})
export class SharedModule { }
